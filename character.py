#!/usr/bin/env python3

from math import ceil
from dice import get_dice

MODES = ["normal", "defense"]
DIFFICULTY = 3
PRIORITY_FACES = 20

class Character:

    xp_caps = {
        1: 0,
        2: 1,
        3: 2,
        4: 3,
        5: 4,
        6: 5,
        7: 6,
        8: 7,
        9: 8,
        10: 9,
        11: 10,
        12: 11,
        13: 12,
        14: 13,
        15: 14,
        16: 15,
        17: 16,
        18: 17,
        19: 18,
        20: 19,
    }


    def __init__(self, name, job, race):
        self.job = job
        self.race = race
        self.mode = "normal"
        self.level = 1
        self.set_health()
        self.set_strength()
        self.set_defense()
        self.set_initiative()
        self.xp = 0
        self.name = name
        self.actions = {
            "attack": self.attack,
            "defend": self.defend,
            "wait": self.wait,
        }

    @property
    def priority(self):
        modifier = self.initiative
        return get_dice(20).roll(1).first + modifier

    @property
    def xp_worth(self):
        return ceil(self.level / 2)

    def set_strength(self):
        self.strength = self.job.strength + self.race.strength

    def set_health(self):
        faces = self.job.health + self.race.health
        dice = get_dice(faces)
        self.health = self.level + dice.roll(1).first

    def set_defense(self):
        self.defense = self.job.defense + self.race.defense

    def set_initiative(self):
        self.initiative = self.job.initiative + self.race.initiative

    @property
    def is_dead(self):
        return self.health <= 0

    def attack(self, opponent):
        print(f"{self.name} attacks {opponent.name}")
        self.mode = "normal"
        d6 = get_dice(6)
        success_roll = d6.roll(1).mean
        difficulty = DIFFICULTY + opponent.get_defense()
        print(f"success roll {success_roll}; difficulty {difficulty}")
        if success_roll < difficulty:
            if success_roll == 1:
                print("EPIC FAIL !!!")
            print("attack missed")
            return
        print("attack successful")
        dmg = 1 + self.strength
        if success_roll == 6:
            print("CRITICAL !!!")
            dmg *= 2
        opponent.takedmg(dmg)

    def defend(self):
        print(f"{self.name} defends")
        self.mode = "defense"

    def levelup(self):
        self.level += 1

    def takedmg(self, dmg):
        self.health -= dmg
        print(f"{self.name} takes {dmg} dmg, new hp {self.health}")

    def wait(self):
        print(f"{self.name} waits")

    def get_defense(self):
        res = self.defense
        if self.mode == "defense":
            res += 1
        return res

    def add_xp(self, qty):
        print(f"{self.name} gained {qty} xp")
        self.xp += qty
        while self.check_level_up():
            self.level_up()

    def check_level_up(self):
        next_level = self.level + 1
        return self.xp >= self.xp_caps[next_level]

    def level_up(self):
        self.level += 1
        print(f"{self.name} reached level {self.level}")
        self.job.apply_bonuses(self.level)
        self.race.apply_bonuses(self.level)
