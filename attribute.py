#!/usr/bin/env python3

class Attribute:

    def __init__(self):
        raise NotImplementedError

    def apply_bonuses(self, level):
        bonuses = self.bonus_by_level(level)
        self.strength += bonuses.get("strength", 0)
        self.health += bonuses.get("health", 0)
        self.initiative += bonuses.get("initiative", 0)
        self.defense += bonuses.get("defense", 0)

    def bonus_by_level(self, level):
        return self.bonuses.get(level, {})
