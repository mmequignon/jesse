#!/usr/bin/env python3

from race import Race

class Elf(Race):

    bonuses = {
        1: {'defense': 0, 'health': 0, 'initiative': 1, 'strength': 0},
        2: {'defense': 0, 'health': 0, 'initiative': 1, 'strength': 0},
        3: {'defense': 0, 'health': 0, 'initiative': 0, 'strength': 1},
        4: {'defense': 0, 'health': 1, 'initiative': 0, 'strength': 0},
        5: {'defense': 0, 'health': 1, 'initiative': 0, 'strength': 0},
        6: {'defense': 1, 'health': 0, 'initiative': 0, 'strength': 0},
        7: {'defense': 0, 'health': 0, 'initiative': 1, 'strength': 0},
        8: {'defense': 0, 'health': 0, 'initiative': 1, 'strength': 0},
        9: {'defense': 0, 'health': 0, 'initiative': 1, 'strength': 0},
        10: {'defense': 0, 'health': 0, 'initiative': 0, 'strength': 1},
        11: {'defense': 0, 'health': 1, 'initiative': 0, 'strength': 0},
        12: {'defense': 1, 'health': 0, 'initiative': 0, 'strength': 0},
        13: {'defense': 0, 'health': 0, 'initiative': 1, 'strength': 0},
        14: {'defense': 0, 'health': 1, 'initiative': 0, 'strength': 0},
        15: {'defense': 0, 'health': 0, 'initiative': 0, 'strength': 1},
        16: {'defense': 0, 'health': 0, 'initiative': 0, 'strength': 1},
        17: {'defense': 0, 'health': 0, 'initiative': 1, 'strength': 0},
        18: {'defense': 1, 'health': 0, 'initiative': 0, 'strength': 0},
        19: {'defense': 0, 'health': 0, 'initiative': 1, 'strength': 0},
        20: {'defense': 1, 'health': 0, 'initiative': 0, 'strength': 0}
    }

    def __init__(self):
        self.defense = 0
        self.strength = 0
        self.health = -1
        self.initiative = 3
