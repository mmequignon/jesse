#!/usr/bin/env python3

from race import Race

class Human(Race):

    bonuses = {
        1: {'defense': 0, 'health': 0, 'initiative': 0, 'strength': 1},
        2: {'defense': 1, 'health': 0, 'initiative': 0, 'strength': 0},
        3: {'defense': 0, 'health': 1, 'initiative': 0, 'strength': 0},
        4: {'defense': 0, 'health': 0, 'initiative': 0, 'strength': 1},
        5: {'defense': 0, 'health': 0, 'initiative': 1, 'strength': 0},
        6: {'defense': 1, 'health': 0, 'initiative': 0, 'strength': 0},
        7: {'defense': 0, 'health': 0, 'initiative': 0, 'strength': 1},
        8: {'defense': 0, 'health': 0, 'initiative': 1, 'strength': 0},
        9: {'defense': 0, 'health': 0, 'initiative': 0, 'strength': 1},
        10: {'defense': 0, 'health': 1, 'initiative': 0, 'strength': 1},
        11: {'defense': 1, 'health': 0, 'initiative': 0, 'strength': 0},
        12: {'defense': 1, 'health': 0, 'initiative': 0, 'strength': 0},
        13: {'defense': 0, 'health': 1, 'initiative': 0, 'strength': 0},
        14: {'defense': 1, 'health': 0, 'initiative': 0, 'strength': 0},
        15: {'defense': 0, 'health': 0, 'initiative': 0, 'strength': 1},
        16: {'defense': 0, 'health': 0, 'initiative': 1, 'strength': 0},
        17: {'defense': 0, 'health': 1, 'initiative': 0, 'strength': 0},
        18: {'defense': 0, 'health': 0, 'initiative': 0, 'strength': 1},
        19: {'defense': 0, 'health': 0, 'initiative': 1, 'strength': 0},
        20: {'defense': 0, 'health': 0, 'initiative': 0, 'strength': 1}
    }

    def __init__(self):
        self.defense = 1
        self.strength = 2
        self.health = 0
        self.initiative = -1
