#!/usr/bin/env python3

from statistics import mean
from math import floor

class DiceResult:

    def __init__(self, result):
        self.result = result

    @property
    def sum(self):
        return sum(self.result)

    @property
    def mean(self):
        return floor(mean(self.result))

    @property
    def first(self):
        return self.result[0]
