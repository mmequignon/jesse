#!/usr/bin/env python3

# char in team

import random

class Fight:

    def __init__(self, team1, team2): # opponents
        self.team1 = team1
        self.team2 = team2
        self.fighters = self.get_ordered_fighters(team1 + team2)

    def team_is_dead(self, team):
        for char in team:
            if not char.is_dead:
                return False
        return True

    def game_is_over(self):
        return self.team_is_dead(self.team1) or self.team_is_dead(self.team2)

    def get_random_opponent(self, fighter):
        if fighter in self.team1:
            opponent_team = self.team2
        else:
            opponent_team = self.team1
        alive_opponents = filter(lambda o: not o.is_dead, opponent_team)
        return random.choice(list(alive_opponents)), opponent_team

    def get_ordered_fighters(self, fighters):
        return sorted(fighters, key=lambda f: f.priority, reverse=True)

    def run(self):
        while "No opponent has 0 hp":
            print("==============")
            for fighter in self.fighters:
                if fighter.is_dead:
                    print(f"{fighter.name} rests its eyes")
                    continue
                opponent, opponent_team = self.get_random_opponent(fighter)
                self.action(fighter, opponent)
                if opponent.is_dead:
                    fighter.add_xp(opponent.xp_worth)
                    if self.team_is_dead(opponent_team):
                        break
            if self.game_is_over():
                print(
                    "\n".join(
                        "\t".join(
                            [
                                f"{f.name}",
                                f"xp: {f.xp}",
                                f"level: {f.level}",
                                f"str: {f.strength}",
                                f"hp: {f.health}",
                                f"init: {f.initiative}",
                                f"def: {f.defense}",
                            ]
                        )
                        for f in self.fighters
                    )
                )
                break

    def action(self, opponent1, opponent2):
        action_name = random.choice(list(opponent1.actions.keys()))
        action = opponent1.actions[action_name]
        if action_name == "attack":
          action(opponent2)
          return
        action()
