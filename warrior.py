#!/usr/bin/env python3

from job import Job

class Warrior(Job):
    
    bonuses = {
        1: {'defense': 0, 'health': 1, 'initiative': 0, 'strength': 0},
        2: {'defense': 0, 'health': 0, 'initiative': 0, 'strength': 1},
        3: {'defense': 0, 'health': 0, 'initiative': 1, 'strength': 0},
        4: {'defense': 1, 'health': 0, 'initiative': 0, 'strength': 0},
        5: {'defense': 0, 'health': 1, 'initiative': 0, 'strength': 0},
        6: {'defense': 0, 'health': 0, 'initiative': 0, 'strength': 1},
        7: {'defense': 0, 'health': 0, 'initiative': 1, 'strength': 0},
        8: {'defense': 1, 'health': 0, 'initiative': 0, 'strength': 0},
        9: {'defense': 1, 'health': 0, 'initiative': 0, 'strength': 0},
        10: {'defense': 0, 'health': 1, 'initiative': 0, 'strength': 1},
        11: {'defense': 0, 'health': 0, 'initiative': 0, 'strength': 1},
        12: {'defense': 0, 'health': 1, 'initiative': 0, 'strength': 0},
        13: {'defense': 0, 'health': 1, 'initiative': 0, 'strength': 0},
        14: {'defense': 0, 'health': 1, 'initiative': 0, 'strength': 0},
        15: {'defense': 1, 'health': 0, 'initiative': 0, 'strength': 0},
        16: {'defense': 0, 'health': 0, 'initiative': 0, 'strength': 1},
        17: {'defense': 0, 'health': 1, 'initiative': 0, 'strength': 0},
        18: {'defense': 0, 'health': 1, 'initiative': 0, 'strength': 0},
        19: {'defense': 0, 'health': 0, 'initiative': 0, 'strength': 1},
        20: {'defense': 0, 'health': 1, 'initiative': 0, 'strength': 1}
    }

    def __init__(self):
        self.defense = 0
        self.strength = 1
        self.health = 8
        self.initiative = -1
