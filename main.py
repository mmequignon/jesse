#!/usr/bin/env python3

from fight import Fight
from character import Character
from human import Human
from dwarf import Dwarf
from elf import Elf
from sorcerer import Sorcerer
from warrior import Warrior
from rogue import Rogue

if __name__ == "__main__":
    team1 = [
        Character("roger", Rogue(), Dwarf()),
        Character("pierre", Sorcerer(), Human()),
        Character("rose", Warrior(), Dwarf()),
        Character("warren", Warrior(), Elf()),
        Character("bob", Rogue(), Human()),
        Character("michel", Sorcerer(), Dwarf()),
    ]
    team2 = [
        Character("hugues", Rogue(), Dwarf()),
        Character("jacques", Sorcerer(), Human()),
        Character("bernard", Warrior(), Dwarf()),
        Character("berty", Warrior(), Elf()),
        Character("gilles", Rogue(), Human()),
        Character("rené", Sorcerer(), Dwarf()),
    ]
    fight = Fight(team1, team2)
    fight.run()
